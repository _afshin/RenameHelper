Example: renamehelper.exe *.srt 'awesome tv show s01e{1}.srt'


this will select all .srt files in current directory and rename them like this:


awesome tv show s01e1.srt

awesome tv show s01e2.srt

awesome tv show s01e3.srt

...



Example: renamehelper.exe *.srt 'awesome tv show s01e{01}.srt'


this will select all .srt files in current directory and rename them like this:


awesome tv show s01e01.srt

awesome tv show s01e02.srt

awesome tv show s01e03.srt

...

awesome tv show s01e09.srt

awesome tv show s01e10.srt

...



Example: renamehelper.exe 'awesome tv show s01e?.srt' 'awesome tv show s01e{01}.srt'


this will add a leading zero to files 'awesome tv show s01e1.srt' through 'awesome tv show s01e9.srt' like this:


awesome tv show s01e01.srt

...





In general the first argument is a regular windows search pattern and the second argument is your desired file name with a placeholder for numeric value.


So {1} means start with number one and increment by one.

{6} means start with number six and increment by one.

{01} means start with number one and increment by one with one left pad.

{004} means start with number four and increment by one with two left pad.


You can also specify your increment step:


{1+2} means start with number one and increment by two.

{6+4} means start with number six and increment by four.

{01+3} means start with number one and increment by three with one left pad.

{004+5} means start with number four and increment by five with two left pad.