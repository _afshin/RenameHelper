using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Rename_Helper
{
    class Program
    {
        private const int ARG_NUM = 2;
        private const string USAGE = "Usage:\nrenamehelper [file pattern] [file name]\n\n"
            + "Example: renamehelper *.srt episode{1}.srt";

        private static string pattern;
        private static string name;
        private static int initNum;
        private static int step;
        private static int leftPad;

        static void Main(string[] args)
        {
            if (args.Length != ARG_NUM)
            {
                Console.WriteLine(USAGE);
                foreach (var arg in args)
                {
                    Console.WriteLine(arg);
                }
                return;
            }

            pattern = args[0];
            InitName(args[1]);
            InitLeadingZero(args[1]);

            var files = new DirectoryInfo(Directory.GetCurrentDirectory())
                .GetFiles(pattern, SearchOption.TopDirectoryOnly);

            foreach (var file in files)
            {
                file.MoveTo(string.Format(name, initNum.ToString().PadLeft(leftPad, '0')));
                initNum += step;
            }
        }

        private static void InitName(string nameArg)
        {
            var namingPattern1 = @"\{\d+\}";
            var namingPattern2 = @"\{\d+-\d+\}";
            var namingPattern3 = @"\{\d+\+\d+\}";

            if (Regex.IsMatch(nameArg, namingPattern1))
            {
                step = 1;

                var subLength = nameArg.IndexOf('}') - nameArg.IndexOf('{') - 1;
                initNum = int.Parse(nameArg.Substring(nameArg.IndexOf('{') + 1, subLength));

                name = Regex.Replace(nameArg, namingPattern1, "{0}");
                return;
            }

            if (Regex.IsMatch(nameArg, namingPattern2))
            {
                var stepLength = nameArg.IndexOf('}') - nameArg.IndexOf('-', nameArg.IndexOf('{')) - 1;
                step = int.Parse(nameArg.Substring(nameArg.IndexOf('-', nameArg.IndexOf('{')) + 1, stepLength)) * -1;

                var subLength = nameArg.IndexOf('-', nameArg.IndexOf('{')) - nameArg.IndexOf('{') - 1;
                initNum = int.Parse(nameArg.Substring(nameArg.IndexOf('{') + 1, subLength));

                name = Regex.Replace(nameArg, namingPattern2, "{0}");
                return;
            }

            if (Regex.IsMatch(nameArg, namingPattern3))
            {
                var stepLength = nameArg.IndexOf('}') - nameArg.IndexOf('+', nameArg.IndexOf('{')) - 1;
                step = int.Parse(nameArg.Substring(nameArg.IndexOf('+', nameArg.IndexOf('{')) + 1, stepLength));

                var subLength = nameArg.IndexOf('+', nameArg.IndexOf('{')) - nameArg.IndexOf('{') - 1;
                initNum = int.Parse(nameArg.Substring(nameArg.IndexOf('{') + 1, subLength));

                name = Regex.Replace(nameArg, namingPattern3, "{0}");
                return;
            }

            Console.WriteLine("No valid pattern was provided.");
            Environment.Exit(160);
        }

        private static void InitLeadingZero(string nameArg)
        {
            int leadingZero;
            var digit = char.Parse(initNum.ToString().Substring(0, 1));

            if (digit != '0')
            {
                leadingZero = nameArg.IndexOf(digit, nameArg.IndexOf('{')) - nameArg.IndexOf('{') - 1;
            }
            else
            {
                leadingZero = -1;
                for (int i = nameArg.IndexOf('{') + 1; i < nameArg.IndexOf('}'); i++)
                {
                    if (nameArg[i] != '0') break;
                    leadingZero++;
                }
            }

            leftPad = leadingZero + initNum.ToString().Length;
        }
    }
}
